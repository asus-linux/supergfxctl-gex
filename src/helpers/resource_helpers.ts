declare const imports: any;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const { Gio, GLib } = imports.gi;

import * as Log from '../modules/log';

/**
 * File abstraction to load files for desired purposes
 */
export class File {
  /**
   * loads DBus XML file and return its content as string (byte[] -> string)
   * @param {string} name filename without extension or path
   * @returns {string|undefined} file content
   */
  public static DBus(name: string): string | undefined {
    const file = `${Me.path}/resources/dbus/${name}.xml`;
    try {
      const [ok, bytes] = GLib.file_get_contents(file);
      if (!ok) Log.warn(`Couldn't read contents of "${file}"`);
      return ok ? imports.byteArray.toString(bytes) : undefined;
    } catch (e) {
      Log.error(`Failed to load "${file}"`, e);
    }
  }
}

/**
 * Icon helpers
 */
export class Icon {
  /**
   * Get a themed icon, using fallbacks from GSConnect's GResource when necessary.
   * @param {string} name - A (themed) icon name or path
   * @return {Gio.Icon} A (themed) icon
   */
  public static getByName(name: string) {
    const iconPath =
      'resource://org/gnome/Shell/Extensions/supergfxctl-gex/icons/scalable';
    const iconNames = [
      'dgpu-active',
      'dgpu-off',
      'dgpu-suspended',
      'gpu-asusmuxdiscreet-active',
      'gpu-asusmuxdiscreet',
      'gpu-compute-active',
      'gpu-compute',
      'gpu-egpu-active',
      'gpu-egpu',
      'gpu-hybrid-active',
      'gpu-hybrid',
      'gpu-integrated-active',
      'gpu-integrated',
      'gpu-nvidianomodeset-active',
      'gpu-nvidianomodeset',
      'gpu-vfio-active',
      'gpu-vfio',
      'reboot',
    ];

    const res: { [key: string]: string } = {};
    for (let iconName of iconNames) {
      res[iconName] = new Gio.FileIcon({
        file: Gio.File.new_for_uri(`${iconPath}/${iconName}.svg`),
      });
    }

    if (res[name] !== undefined) return res[name];
    return new Gio.ThemedIcon({ name: name });
  }
}
