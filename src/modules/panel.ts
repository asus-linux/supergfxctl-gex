declare const imports: any;
// @ts-ignore: Me must be declared (even if unused) to allow external imports
const Me = imports.misc.extensionUtils.getCurrentExtension();

const { GObject } = imports.gi;
const { popupMenu } = imports.ui;
const { QuickMenuToggle, SystemIndicator } = imports.ui.quickSettings;

import * as Resources from '../helpers/resource_helpers';
import { DBus } from './dbus';
import { LabelHelper, LabelType } from '../helpers/label_helper';

// export const Title = 'supergfxctl';

/**
 * indicator icon
 */
export const gpuModeIndicator = GObject.registerClass(
  class gpuModeIndicator extends SystemIndicator {
    constructor() {
      super();
    }
  }
);

/**
 * menu toggle (QuickToggle)
 */
export const gpuModeToggle = GObject.registerClass(
  class gpuModeToggle extends QuickMenuToggle {
    private _dbus?: DBus;
    private _labels?: LabelHelper;
    private _gpuModeIndicator: any = null;
    private _gpuModeItems = new Map();

    constructor(gpuModeIndicator: any) {
      super();

      // set DI and initialize indicator
      this._gpuModeIndicator = gpuModeIndicator;

      // initialize the indicator
      this._gpuModeIndicator._indicator =
        this._gpuModeIndicator._addIndicator();
      this._gpuModeIndicator._indicator.visible = true;

      // populate menu
      this._gpuModeSection = new popupMenu.PopupMenuSection();
      this.menu.addMenuItem(this._gpuModeSection);

      // initialize DBus
      this._dbus = new DBus(this); // this also spawns the quick-toggle title and labels
      this._labels = this._dbus.labelHelper; // encapsulation of dbus labels (no glob const anymore!)
      this.refresh(); // initial refresh of the states
    }

    /**
     * reloads the UI if needed (labels, icons, toggles, etc.)
     */
    public refresh(): void {
      // avoid race-condition on signal change during init
      if (this._dbus === undefined || this._labels === undefined) return;

      this._gpuModeSection.removeAll();
      this._gpuModeItems.clear();

      const lastState = this._dbus.lastState;

      for (const key in this._dbus.supported) {
        if (Object.prototype.hasOwnProperty.call(this._dbus.supported, key)) {
          const element = this._dbus.supported[key];
          const item = new popupMenu.PopupImageMenuItem(
            this._labels.get(LabelType.GfxMenu, element),
            Resources.Icon.getByName(
              `gpu-${this._labels.get(LabelType.Gfx, element)}`
            )
          );
          item.connect('activate', () => {
            if (this._dbus !== undefined) this._dbus.gfxMode = element;
          });
          this._gpuModeItems.set(element, item);
          this._gpuModeSection.addMenuItem(item);
        }
      }

      this._gpuModeSection.addMenuItem(new popupMenu.PopupSeparatorMenuItem());

      for (const [profile, item] of this._gpuModeItems) {
        item.setOrnament(
          profile === lastState
            ? popupMenu.Ornament.DOT
            : popupMenu.Ornament.NONE
        );
      }

      let powerItem = new popupMenu.PopupImageMenuItem(
        `dedicated GPU is ${this._labels.get(
          LabelType.Power,
          this._dbus.lastPowerState
        )}`,
        Resources.Icon.getByName(
          `dgpu-${this._labels.get(
            LabelType.PowerFileName,
            this._dbus.lastPowerState
          )}`
        )
      );

      powerItem.sensitive = false;
      powerItem.active = false;

      this._gpuModeSection.addMenuItem(powerItem);

      const title = `${this._labels.get(
        LabelType.GfxMenu,
        this._dbus.lastState
      )} (${this._labels.get(LabelType.Power, this._dbus.lastPowerState)})`;

      if (this._labels.gsVersion >= 44) this.title = title;
      else this.label = title;

      const icon = Resources.Icon.getByName(
        `gpu-${this._labels.get(LabelType.Gfx, this._dbus.lastState)}${
          this._labels.get(
            LabelType.PowerFileName,
            this._dbus.lastPowerState
          ) == 'active'
            ? '-active'
            : ''
        }`
      );
      this.gicon = icon;

      this.addIndicator();

      this.menu.setHeader(
        icon,
        `${this._labels.get(
          LabelType.GfxMenu,
          this._dbus.lastState
        )} (${this._labels.get(LabelType.Power, this._dbus.lastPowerState)})`
      );
    }

    /**
     * adds the indicator icon
     */
    public addIndicator(): void {
      if (this._dbus === undefined || this._labels === undefined) return;

      this._gpuModeIndicator.remove_actor(this._gpuModeIndicator._indicator);
      this._gpuModeIndicator._indicator.gicon = Resources.Icon.getByName(
        `gpu-${this._labels.get(LabelType.Gfx, this._dbus.lastState)}${
          this._labels.get(
            LabelType.PowerFileName,
            this._dbus.lastPowerState
          ) == 'active'
            ? '-active'
            : ''
        }`
      );
      this._gpuModeIndicator._indicator.style_class =
        'supergfxctl-gex panel-icon system-status-icon';

      this._gpuModeIndicator.add_actor(this._gpuModeIndicator._indicator);
    }

    /**
     * remove from panel
     */
    public disable(): void {
      this.destroy();

      this._dbus = undefined;
      this._labels = undefined;
      this._gpuModeIndicator = null;
    }
  }
);
