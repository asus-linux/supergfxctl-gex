declare const imports: any;
// @ts-ignore: Me must be declared (even if unused) to allow external imports
const Me = imports.misc.extensionUtils.getCurrentExtension();
const { Gio } = imports.gi;

import * as Resources from '../helpers/resource_helpers';
import * as Log from './log';
import { VersionNumber } from '../helpers/version_helper';
import { PanelHelper } from '../helpers/panel_helper';
import { LabelHelper, LabelType } from '../helpers/label_helper';

/**
 * DBus implementation for supergfxd (supergfxctl)
 */
export class DBus {
  public labels!: LabelHelper;
  private gpuModeToggle!: any; // caller DI instance(parent)
  private proxy!: any;
  private sgfxVendor = '';
  private sgfxLastState = 6; // 6 is unknonwn (+1 if v5.1.1)
  private sgfxPowerState = 3; // 3 is off
  private sgfxSupportedGPUs: number[] = [];

  public connected = false;

  constructor(gpuModeToggle: any) {
    this.gpuModeToggle = gpuModeToggle;
    this.proxy = new Gio.DBusProxy.makeProxyWrapper(
      Resources.File.DBus('org-supergfxctl-gfx-5')
    )(Gio.DBus.system, 'org.supergfxctl.Daemon', '/org/supergfxctl/Gfx');

    // initialize it
    if ((this.connected = this.supportedMode) && this.initialize()) {
      // bind QuickToggles (as a result)
      try {
        const lastState = this.lastState;
        const lastPState = this.lastPowerState;
        const lVar = `${this.labels.get(
          LabelType.GfxMenu,
          lastState
        )} (${this.labels.get(LabelType.Power, lastPState)}})`;
        const gpuIconName = `gpu-${
          this.labels.get(LabelType.Gfx, lastState) +
            this.labels.get(LabelType.PowerFileName, lastPState) ==
          'active'
            ? '-active'
            : ''
        }`;

        if (this.labels.gsVersion >= 44) this.gpuModeToggle.title = lVar;
        else this.gpuModeToggle.label = lVar;

        this.gpuModeToggle.gicon = Resources.Icon.getByName(gpuIconName);
        this.gpuModeToggle.menu.setHeader(
          Resources.Icon.getByName(gpuIconName),
          `${this.labels.get(LabelType.GfxMenu, lastState)} (${this.labels.get(
            LabelType.Power,
            lastPState
          )})`
        );
      } catch (e) {
        PanelHelper.notify(
          'Could not establish connection to supergfxctl!',
          'gpu-integrated-active',
          '',
          3
        );

        Log.error(
          'Graphics Mode DBus initialization using supergfxctl failed after making the connection!',
          e
        );
      }
    }
  }

  /**
   * initializes the DBus interfaces and signals (must be called first befora, usage!)
   * @returns {boolean} success/error
   */
  public initialize(): boolean {
    Log.info('initializing DBus interface...');
    try {
      this.initLabels();
      // adjustments if v5.1.1 or above
      if (this.labels.v51) {
        this.sgfxLastState = 7;
      }

      // calling public getter (without assignment to prefill privates)
      this.vendor;
      this.gfxMode;
      this.gpuPower;

      // connect signals (DBus)
      this.connectSignals();
    } catch {
      Log.error('initializing DBus failed!');
      return false;
    }
    Log.info('initialized DBus interface.');
    return true;
  }

  private connectSignals(): void {
    this.connectNotifySignal();
    this.connectPowerSignal();
  }

  /**
   * Gets and checks list of supported modes
   * @returns {boolean} yes/no
   */
  private get supportedMode(): boolean {
    const supp = this.proxy.SupportedSync();
    if (Array.isArray(supp) && Array.isArray(supp[0])) {
      this.sgfxSupportedGPUs = supp[0].map((n) => parseInt(n) || 0);
      return supp[0].length === this.sgfxSupportedGPUs.length;
    }

    Log.warn('Graphics Mode DBus: get current mode failed!');
    return false;
  }

  /**
   * initializes the labels (needed for version compatibility)
   */
  public initLabels(): void {
    try {
      // instanciate with a version, instead of a VersionHelper instance
      this.labels = new LabelHelper(
        this.proxy.VersionSync().toString().split('.') as VersionNumber
      );
    } catch (e) {
      Log.error('Graphics Mode DBus: get current version failed!', e);
    }
  }

  /**
   * connects to the gfx-change notify signal-bus (DBus)
   */
  public connectNotifySignal(): void {
    try {
      this.proxy.connectSignal(
        'NotifyAction',
        // @ts-ignore
        (proxy: any = null, name: string, value: number) => {
          let newMode = parseInt(this.proxy.ModeSync());
          let details = `Graphics Mode has changed.`;
          let icon = 'gpu-integrated-active';
          let switchable = true;

          switch (this.labels.get(LabelType.UserAction, value)) {
            case 'integrated':
              details = `You must switch to Integrated mode before switching to VFIO.`;
              switchable = false;
              break;
            case 'none':
              details = `Graphics Mode changed to ${this.labels.get(
                LabelType.Gfx,
                newMode
              )}`;
              break;
            default:
              details = `Graphics Mode changed. Please save your work and ${this.labels.get(
                LabelType.UserAction,
                value
              )} to apply the changes.`;
              icon = 'reboot';
              break;
          }
          if (switchable && newMode !== this.sgfxLastState) {
            this.sgfxLastState = newMode;
            this.gpuModeToggle.refresh();
          }

          PanelHelper.notify(
            details,
            icon,
            this.labels.get(LabelType.UserAction, value)
          );
        }
      );
    } catch (error) {
      Log.error(
        `Error connecting Signal, no live updates of GPU modes!`,
        error
      );
    }
  }

  /**
   * connecting DBus to the power-signal provider/generator
   */
  public connectPowerSignal(): void {
    try {
      this.proxy.connectSignal(
        'NotifyGfxStatus',
        // @ts-ignore
        (proxy: any = null, name: string, value: number[]) => {
          if (this.sgfxPowerState !== value[0]) {
            this.sgfxPowerState = value[0];

            // if integrated and active show notification
            if (this.sgfxPowerState == 0 && this.sgfxLastState == 1) {
              // let's check the mode
              try {
                let mode = parseInt(this.proxy.ModeSync());
                if (this.labels.is(LabelType.Gfx, mode, 'integrated'))
                  PanelHelper.notify(
                    `Your dedicated GPU turned on while you are on the integrated mode. This should not happen. It could be that another application rescanned your PCI bus. Rebooting is advised.`,
                    'gif/fire.gif',
                    'reboot'
                  );
                else if (this.sgfxLastState !== mode) this.sgfxLastState = mode;
              } catch (e) {
                Log.error('Graphics Mode DBus getting mode failed!', e);
              }
            }
            this.gpuModeToggle.refresh();
          }
        }
      );
    } catch (error) {
      Log.error(
        `Error connecting Signal, no live updates of GPU modes!`,
        error
      );
    }
  }

  /**
   * get Vendor from DBus
   * @returns {string} the current gfx-vendor using DBus
   */
  public get vendor(): string {
    try {
      return (this.sgfxVendor = this.proxy.VendorSync());
    } catch (e) {
      Log.error('Graphics Mode DBus: get current vendor failed!', e);
    }
    return this.sgfxVendor;
  }

  /**
   * get GfxMode from DBus
   * @returns {number} the gfx-mode using DBus
   */
  public get gfxMode(): number {
    try {
      return (this.sgfxLastState = parseInt(this.proxy.ModeSync()));
    } catch (e) {
      Log.error('Graphics Mode DBus: get current mode failed!', e);
    }
    return this.sgfxLastState;
  }

  /**
   * set the desired gfx-mode
   * @param {number} mode gfx-mode to set (id)
   */
  public set gfxMode(mode: number) {
    try {
      this.proxy.SetModeSync(mode);
    } catch (e: any) {
      Log.error('Graphics Mode DBus switching failed!', e);
      PanelHelper.notify(e.toString(), 'gif/fire.gif');
    }
  }

  /**
   * get current PowerMode from DBus
   * @returns {number} last gpu-power-state using DBus
   */
  public get gpuPower(): number {
    try {
      this.sgfxPowerState = parseInt(this.proxy.PowerSync().toString().trim());
    } catch (e) {
      Log.error('Graphics Mode DBus getting power mode failed!', e);
      this.sgfxPowerState = 3;
    }

    return this.sgfxPowerState;
  }

  /**
   * get supported GPUs
   * @returns {number[]} supported gpu-id list, using DBus
   */
  public get supported(): number[] {
    return this.connected ? this.sgfxSupportedGPUs : [];
  }

  /**
   * get the last GfxState
   * @returns {number} the last gfx-state
   */
  public get lastState(): number {
    return this.connected ? this.sgfxLastState : 6 + (this.labels.v51 ? 1 : 0);
  }

  /**
   * get the last GfxPowerState
   * @returns {number} the last power-state
   */
  public get lastPowerState(): number {
    return this.connected ? this.sgfxPowerState : 3;
  }

  /**
   * get the labelHelper instance
   * @returns {LabelHelper} instance used by DBus (shared)
   */
  public get labelHelper(): LabelHelper {
    return this.labels;
  }
}
