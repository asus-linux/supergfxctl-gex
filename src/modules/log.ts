declare const log: any, logError: any;

/**
 * RAW Logging abstraction
 * @param {string} text log message
 * @param {string} prefix a variable prefix like '[TEST]' (optional)
 * @param {any} obj an object to dump (optional)
 */
export function raw(text: string, prefix: string = '', obj: any = null): void {
  if (obj !== null) {
    log(`supergfxctl-gex: ${prefix} ${text}\nobj:\n${JSON.stringify(obj)}`);
  } else {
    log(`supergfxctl-gex: ${prefix} ${text}`);
  }
}

/**
 * logs a message with "info" level
 * @param {string} text log message
 * @param {any} obj an object to dump (optional)
 */
export function info(text: string, obj: any = null) {
  raw(text, '[INFO]', obj);
}

/**
 * logs an error message
 * @param {string} text message
 * @param {any} e exception object
 */
export function error(text: string, e: any = null) {
  logError(e, `supergfxctl-gex: ${text}`);
}

/**
 * logs a message with "warn" level
 * @param {string} text log message
 * @param {any} obj an object to dump (optional)
 */
export function warn(text: string, obj: any = null) {
  raw(text, '[WARN]', obj);
}

/**
 * logs a message with "debug" level
 * @param {string} text log message
 * @param {any} obj an object to dump (optional)
 */
export function debug(text: string, obj: any = null) {
  raw(text, '[DEBUG]', obj);
}
